"use client";
import React, { useRef, useCallback, useEffect } from "react";
import Webcam from "react-webcam";

export default function RecordPacking() {
  const webcamRef = useRef<Webcam | null>(null);
  const mediaRecorderRef = useRef<MediaRecorder | null>(null);
  const [capturing, setCapturing] = React.useState<boolean>(false);
  const [recordedChunks, setRecordedChunks] = React.useState<Blob[]>([]);
  const [refcodeData, setRefcodeData] = React.useState<string>("");

  const handleKeyStart = (event: any) => {
    if (event.key === "Enter") {
      setRefcodeData(event.target.value);
      handleStartCaptureClick();
    }
  };
  useEffect(() => {
    if (refcodeData) {
      const handleKeyDown = (event: any) => {
        if (event.key === "Enter") {
          handleStopCaptureClick();
        }
      };
      document.addEventListener("keydown", handleKeyDown);
      return () => {
        document.removeEventListener("keydown", handleKeyDown);
      };
    }
  }, [refcodeData]);

  const handleStartCaptureClick = React.useCallback(() => {
    setCapturing(true);
    const camRefCurrent = webcamRef.current;
    const camRefVideo = camRefCurrent?.video;
    const camRef = camRefVideo?.srcObject as MediaStream;

    mediaRecorderRef.current = new MediaRecorder(camRef, {
      mimeType: "video/webm",
    });
    mediaRecorderRef.current.addEventListener("dataavailable", (event) => {
      // console.log("handleStartCaptureClick Data available:", event.data);
      handleDataAvailable(event);
    });
    mediaRecorderRef.current.start();
  }, [webcamRef, setCapturing, mediaRecorderRef]);

  const handleDataAvailable = React.useCallback(
    ({ data }: { data: Blob }) => {
      // console.log("handleDataAvailable Data available:", data);
      if (data.size > 0) {
        setRecordedChunks((prev) => prev.concat(data));
      }
    },
    [setRecordedChunks],
  );

  const handleStopCaptureClick = React.useCallback(async () => {
    mediaRecorderRef.current?.stop();
    setCapturing(false);
  }, [mediaRecorderRef, webcamRef, setCapturing]);

  useEffect(() => {
    if (recordedChunks.length) {
      handleDownload();
    }
  }, [recordedChunks]);

  const handleDownload = useCallback(async () => {
    if (recordedChunks.length) {
      try {
        const fetchData = async () => {
          const blob = new Blob(recordedChunks, { type: "video/webm" });
          const formData = new FormData();
          formData.append("video", blob);
          formData.append("refcode", refcodeData);

          const response = await fetch("api/upload/video", {
            method: "POST",
            body: formData,
          });

          if (!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);
          }
          setRecordedChunks([]);
        };

        fetchData().catch((e) => {
          console.error("fetchData : พัง ", e);
        });
      } catch (error) {
        console.error("error Vdo พัง :", error);
      }
    }
  }, [recordedChunks]);

  return (
    <div className="text-center m-5 gap-5">
      <h1 className="text-2xl">บันทึกการแพ็คสินค้า</h1>
      <Webcam audio={false} ref={webcamRef} />
      {!capturing ? (
        <div>
          <input
            autoFocus
            type="text"
            onKeyDown={handleKeyStart}
            className="bg-gray-200 p-2 rounded-lg w-full max-w-96 my-10"
            placeholder="กรอกหมายเลขอ้างอิง"
          />
        </div>
      ) : (
        <div>
          <button onClick={handleStopCaptureClick}>stop Capture</button>
        </div>
      )}

      {recordedChunks.length > 0 && (
        <div></div>
        // <div> <button onClick={handleDownload}>Download { recordedChunks.length }</button></div>
      )}
    </div>
  );
}
