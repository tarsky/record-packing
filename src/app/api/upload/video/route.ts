import { NextRequest } from "next/server";
import fs from 'fs/promises'
async function checkFileExists(filePath : string) {
    try {
        // ตรวจสอบคุณสมบัติของไฟล์
        await fs.stat(filePath);
        return true; // ถ้ามีไฟล์อยู่
    } catch (error : any) {
        if (error.code === 'ENOENT') {
            return false; // ถ้าไม่มีไฟล์
        } else {
            throw error; // ถ้าเกิดข้อผิดพลาดอื่นๆ
        }
    }
}

export async function POST( request: NextRequest) {
    
    try {
      const formData = await request.formData();
      const videoFile = formData.get('video');
      const refcode = formData.get('refcode');
      
        console.log(videoFile)
      if (!videoFile) {
        return new Response('No video file found', { status: 400 });
      }
      const uploadFolder = 'public/upload/';
      const path_refcode = uploadFolder+refcode
      const fileExists = await checkFileExists(path_refcode);
  
      await fs.mkdir(uploadFolder+refcode, { recursive: true });
     
      const filePath = `public/upload/`+refcode+`/`+refcode+`.webm`// ระบุตำแหน่งที่ต้องการบันทึกไฟล์
      await fs.writeFile(filePath, Buffer.from(await videoFile.arrayBuffer()), { flag: 'w' });
  
      return new Response('Video file uploaded successfully'+videoFile, { status: 200 });
    } catch (error) {
      return new Response(`Failed to upload video file: ${error.message}`, { status: 500 });
    }
  }