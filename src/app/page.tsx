import RecordPacking from "@/components/recordpacking";
import Image from "next/image";

export default function Home() {
  return (
    <>
      <div className="container mx-auto">
        <div className="flex flex-col justify-center items-center">
          <RecordPacking />
        </div>
      </div>
    </>
  );
}
